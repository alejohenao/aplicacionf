import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ICompras } from './Compras';

@Injectable({
  providedIn: 'root'
})
export class ComprasService {

  constructor(private http:HttpClient) { }
  ListarCompras():Observable <ICompras[]>{
    return this.http.get<ICompras[]>(`${environment.END_POINT}/api/compra/`)
    .pipe(map(res =>{
      return res;
    }));
  }
  public CrearCompra(compra: ICompras):Observable<ICompras>{
    return this.http.post<ICompras>(`${environment.END_POINT}/api/compra/`,compra)
    .pipe(map(res => {
      console.warn('compra',compra)
      return res;
    }));
  };
}
