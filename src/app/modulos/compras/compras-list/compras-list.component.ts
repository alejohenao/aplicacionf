import { Component, OnInit } from '@angular/core';
import { ICompras } from '../Compras';
import { ComprasService } from '../compras.service';

@Component({
  selector: 'app-compras-list',
  templateUrl: './compras-list.component.html',
  styleUrls: ['./compras-list.component.styl']
})
export class ComprasListComponent implements OnInit {
  p: number=1;
  listadoCompras:ICompras[];
  constructor(private service:ComprasService) { }

  ngOnInit(): void {
    this.service.ListarCompras().subscribe(res =>{
      console.warn('listado', res);
      this.listadoCompras=res;
    });
  }

}
