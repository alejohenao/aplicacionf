import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ComprasCreateComponent } from './compras-create/compras-create.component';
import { ComprasListComponent } from './compras-list/compras-list.component';
import { ComprasViewComponent } from './compras-view/compras-view.component';
import { ComprasComponent } from './compras.component';

const routes: Routes = [
  {
    path:'compras-view',
    component:ComprasViewComponent
  },
  {
    path:'compras-create',
    component:ComprasCreateComponent
  },
  {
    path:'compras-list',
    component:ComprasListComponent
  },
  {
    path:'compras',
    component:ComprasComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComprasRoutingModule { }
