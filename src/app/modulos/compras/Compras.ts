import { IProducto, Producto} from "../productos/Producto";
import { IUsuarios, Usuarios } from "../usuarios/Usuarios";


export interface ICompras{
    id?:number;
    fecha?:string;
    cantidad?:string;
    usuarios?:IUsuarios;
    producto?:IProducto;
    codigo?:IProducto;
    documento?:IUsuarios;
}

export class Compras implements ICompras{
    constructor(
        public id?:number,
        public fecha?:string,
        public cantidad?:string,
        public usuarios?:IUsuarios,
        public producto?:IProducto,
        public codigo?:IProducto,    
        public documento?:IUsuarios
    ){}
}