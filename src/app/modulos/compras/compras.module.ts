import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComprasRoutingModule } from './compras-routing.module';
import { ComprasViewComponent } from './compras-view/compras-view.component';
import { ComprasCreateComponent } from './compras-create/compras-create.component';
import { ComprasListComponent } from './compras-list/compras-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { ComprasComponent } from './compras.component';


@NgModule({
  declarations: [
    ComprasViewComponent,
    ComprasCreateComponent,
    ComprasListComponent,
    ComprasComponent
  ],
  imports: [
    CommonModule,
    ComprasRoutingModule,  
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule
  ],
  exports: [
    ComprasViewComponent,
    ComprasCreateComponent,
    ComprasListComponent]
})
export class ComprasModule { }
