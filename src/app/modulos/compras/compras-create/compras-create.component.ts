import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { IProducto } from '../../productos/Producto';
import { ProductoService } from '../../productos/producto.service';
import { IUsuarios } from '../../usuarios/Usuarios';
import { UsuariosService } from '../../usuarios/usuarios.service';
import { ICompras } from '../Compras';
import { ComprasService } from '../compras.service';

@Component({
  selector: 'app-compras-create',
  templateUrl: './compras-create.component.html',
  styleUrls: ['./compras-create.component.styl']
})
export class ComprasCreateComponent implements OnInit {
  codigo:IProducto[];
  documento:IUsuarios[];
  compra:ICompras;
  constructor(private service:ComprasService,private productoService:ProductoService,private usuariosService: UsuariosService,private fb:FormBuilder,
              private route:Router) { }
    compraForm= this.fb.group({
      codigo:[''],
      cantidad:[''],
      documento:['']
    });
  ngOnInit(): void {
  }
  //metodo guardar
  Guardar(): void{
      this.service.CrearCompra(this.compraForm.value).subscribe(res =>{
        console.warn('mensaje de compra',res);
        this.route.navigate(['compras-list']);
        
      });
      
      this.compraForm.reset();
  }

}
