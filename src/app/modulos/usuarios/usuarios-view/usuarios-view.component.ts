import { Component, OnInit } from '@angular/core';
import { UsuariosService } from '../usuarios.service';
import { IUsuarios } from '../Usuarios';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-usuarios-view',
  templateUrl: './usuarios-view.component.html',
  styleUrls: ['./usuarios-view.component.styl']
})
export class UsuariosViewComponent implements OnInit {

  usuario:IUsuarios;
  usuariosLista:IUsuarios[] =[];

  
  searchForm = this.fb.group({
    documento:['',[Validators.pattern('[0-9.]*')]],
    nombre:''
  });
  constructor(private fb: FormBuilder,
              private usuariosService: UsuariosService
  ) {}

ngOnInit(): void {}

searchUsuario(): void { 
  console.warn('documento', this.searchForm.value.documento);
  this.usuariosService.BuscarDocumentoUsuarios(this.searchForm.value.documento)
  .subscribe(res => {
    console.warn('respuesta', res);
    this.usuario = res[0];
  });
}

}
