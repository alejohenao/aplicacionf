import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsuariosComponent } from './usuarios.component';
import { UsuariosCreateComponent } from './usuarios-create/usuarios-create.component';
import { UsuariosListComponent } from './usuarios-list/usuarios-list.component';
import { UsuariosUpdateComponent } from './usuarios-update/usuarios-update.component';
import { UsuariosViewComponent } from './usuarios-view/usuarios-view.component';

const routes: Routes = [
  {
    path:'usuarios-create',
    component:UsuariosCreateComponent
  },
  {
    path:'usuarios-list',
    component:UsuariosListComponent
  },
  {
    path:'usuarios-update',
    component:UsuariosUpdateComponent
  },
  {
    path:'usuarios-view',
    component:UsuariosViewComponent
  },
  {
    path:'usuarios',
    component:UsuariosComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuariosRoutingModule { }
