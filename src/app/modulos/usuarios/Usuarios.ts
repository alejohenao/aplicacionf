export interface IUsuarios{
    id?:number;
    documento?:string;
    nombre?:string;
    apellido?:string;
    direccion?:string;
    telefono?:string;
}

export class Usuarios implements IUsuarios{  
    constructor(
           public id?:number,
           public documento?:string,
           public nombre?:string,
           public apellido?:string,
           public direccion?:string,
           public telefono?:string
            ){}
        
}
