import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsuariosRoutingModule } from './usuarios-routing.module';
import { UsuariosCreateComponent } from './usuarios-create/usuarios-create.component';
import { UsuariosListComponent } from './usuarios-list/usuarios-list.component';
import { UsuariosUpdateComponent } from './usuarios-update/usuarios-update.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { UsuariosViewComponent } from './usuarios-view/usuarios-view.component';
import { UsuariosComponent } from './usuarios.component';
import { NgxPaginationModule } from 'ngx-pagination';





@NgModule({
  declarations: [
    UsuariosCreateComponent,
    UsuariosListComponent,
    UsuariosUpdateComponent,
    UsuariosViewComponent,
    UsuariosComponent
  ],
  imports: [
    CommonModule,
    UsuariosRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule
  ],
  exports:[ 
    UsuariosCreateComponent,
    UsuariosListComponent,
    UsuariosUpdateComponent,
    UsuariosComponent,
    UsuariosViewComponent]
})
export class UsuariosModule { }
