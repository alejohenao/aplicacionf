import { Component, OnInit } from '@angular/core';
import { UsuariosService } from '../usuarios.service';
import { IUsuarios } from '../Usuarios';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-usuarios-list',
  templateUrl: './usuarios-list.component.html',
  styleUrls: ['./usuarios-list.component.styl']
})
export class UsuariosListComponent implements OnInit {  
  p: number = 1;
  listadoUsuarios:IUsuarios[];
  constructor(private service: UsuariosService) { }

  //metodo para listar  
  ngOnInit(): void {
   this.service.ListarUsuarios().subscribe(res => {
      this.listadoUsuarios=res;
    });
  }

  Eliminar(id:number){
   console.log("mensaje de eliminar",)

   const swalWithBootstrapButtons = Swal.mixin({
    customClass: {confirmButton:'btn btn-success',cancelButton: 'btn btn-danger'},
    buttonsStyling: false
  })  
  swalWithBootstrapButtons.fire({
    title:'Estas seguro?',text:"¡No podrás revertir esto!",icon:'warning',
    showCancelButton:true,confirmButtonText:'Si, eliminar!',cancelButtonText:'No, cancelar!',
    reverseButtons:true,background:'#f8fffb'}).then((result) => {
    if (result.isConfirmed) {
      swalWithBootstrapButtons.fire('Eliminando!','Se a eliminado.','success')
    } else if ( 
      result.dismiss === Swal.DismissReason.cancel
    ) {
      swalWithBootstrapButtons.fire(
        'Cancelando','Se a cancelado.','error')
    }
  })

    this.service.EliminarUsuarios(id).subscribe(() =>{          
    this.ngOnInit();
    });  
    }     
}
