import { Injectable } from '@angular/core';
import { IUsuarios } from './Usuarios';
import { environment } from 'src/environments/environment';
import { Observable, pipe } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { createRequestParams } from 'src/app/utils/utils';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  constructor(private http:HttpClient) { }

  //metodo para listar personas se comunica con el backend
  ListarUsuarios():Observable <IUsuarios[]>{
    return this.http.get<IUsuarios[]>(`${environment.END_POINT}/api/usuarios`)
    .pipe(map(res => {
        return res;
      }));  
      }

  //metodo para crear personas se comunica con el backend
  public CrearUsuarios(usuario: IUsuarios):Observable<IUsuarios>{
    return this.http.post<IUsuarios>(`${environment.END_POINT}/api/usuarios`,usuario)
    .pipe(map(res => {
     console.warn('usuario',usuario)
     return res;       
    }));
    }; 
//metodo para actualizar personas se comunica con el backend
ActualizarUsuarios(usuarios:IUsuarios):Observable<IUsuarios>{
return this.http.put<IUsuarios>(`${environment.END_POINT}/api/usuarios`,usuarios)
.pipe(map(res =>{
  return res;
}));
}
//metodo para listar id personas se comunica con el backend
ListarId(id: string):Observable<IUsuarios>{
  return this.http.get<IUsuarios>(`${environment.END_POINT}/api/usuarios/${id}`)
  .pipe(map(res => {
    return res;
  }));
}
//metodo para eliminar personas se comunica con el backend
EliminarUsuarios(id:number):Observable<IUsuarios>{
  return this.http.delete<IUsuarios>(`${environment.END_POINT}/api/usuarios/${id}`)
  .pipe(map(res =>{
    return res;
  }));
}
//metodo para buscar por documento se comunica con el backend
public BuscarDocumentoUsuarios(documento: string): Observable<IUsuarios[]>{
  let params: HttpParams = new HttpParams();
  params = params.set('documento', documento);
  return this.http.get<IUsuarios[]>(`${environment.END_POINT}/api/usuarios/buscarDocumentoUsuario`,{params: params})
  .pipe(map(res =>{
    return res;
  }));
}
}
