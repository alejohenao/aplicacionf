import { Component, OnInit } from '@angular/core';
import { UsuariosService } from '../usuarios.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-usuarios-create',
  templateUrl: './usuarios-create.component.html',
  styleUrls: ['./usuarios-create.component.styl']
})
export class UsuariosCreateComponent implements OnInit {

  constructor(private service:UsuariosService, private fb:FormBuilder,
              private route: Router) { }

  usuariosForm = this.fb.group({
    documento:['',[ Validators.pattern('[0-9.]*'),
                    Validators.required]],
    nombre:['',Validators.required],
    apellido:['',Validators.required],
    direccion:['',Validators.required],
    telefono:['',[ Validators.pattern('[0-9.]*'),
                   Validators.required]],
  })
  ngOnInit(): void {
  }
 //metodo para guardar datos 
 Guardar(): void{
  Swal.fire({toast:true,padding:'1.5em',icon:'success',title:'Nuevo usuario creado',
  timerProgressBar:true,showConfirmButton:false,timer:1000,iconColor:'blue',background:'#e5fbff'})
   this.service.CrearUsuarios(this.usuariosForm.value).subscribe(res =>{
     console.warn('mensaje de la creacion',res);
     //lleva a la ruta usuarios list despues de haber creado
     this.route.navigate(['usuarios-list']);
   });
   //resetear formulario
   this.usuariosForm.reset();
   
 }
}
