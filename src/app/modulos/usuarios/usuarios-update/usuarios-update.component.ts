import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuariosService } from '../usuarios.service';
import { IUsuarios } from '../Usuarios';
import { FormGroup,FormBuilder } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-usuarios-update',
  templateUrl: './usuarios-update.component.html',
  styleUrls: ['./usuarios-update.component.styl']
})
export class UsuariosUpdateComponent implements OnInit {
  formUsuariosUpdate :FormGroup;
  constructor(
    private activatedRoute: ActivatedRoute,
    private services: UsuariosService,
    private formBuilder: FormBuilder,
    private router: Router) { 

      this.formUsuariosUpdate =this.formBuilder.group({
          id:[],
          documento:[''],
          nombre:[''],
          apellido:[''],
          direccion:[''],
          telefono:['']
          });
          }

  ngOnInit(): void{
   let id = this.activatedRoute.snapshot.paramMap.get('id');
   console.warn('ID',id);
    this.services.ListarId(id)
    .subscribe(res => {
    console.warn('mensaje de id',res);
     this.loadForm(res);
  }, res => {});
  }
  //funcion para traer y carga los datos de personas
 private loadForm(usuarios:IUsuarios){
   this.formUsuariosUpdate.patchValue({
     id:usuarios.id,
     documento:usuarios.documento,
    nombre:usuarios.nombre,
     apellido:usuarios.apellido,
     direccion:usuarios.direccion,
     telefono:usuarios.telefono
   });
}
//metodo para actualizar
Actualizar():void{
  Swal.fire({toast:true,padding:'1.5em',icon:'success',title:'actualizacion realizada',
  timerProgressBar:true,showConfirmButton:false,timer:1500,iconColor:'blue',background:'#e5fbff'})
  this.services.ActualizarUsuarios(this.formUsuariosUpdate.value)
  .subscribe(res =>{
    console.warn('actualizacion',res);
    this.router.navigate(['./homepage/menu/usuarios/usuarios-list']);
  }, err => {});
}
}
