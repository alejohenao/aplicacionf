import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { ProductoService } from '../producto.service';

@Component({
  selector: 'app-producto-create',
  templateUrl: './producto-create.component.html',
  styleUrls: ['./producto-create.component.styl']
})
export class ProductoCreateComponent implements OnInit {

  constructor(private service:ProductoService, private fb:FormBuilder,private route:Router) { }

  productoForm = this.fb.group({
    codigo:['',[ Validators.pattern('[0-9.]*'),
    Validators.required]],
    nombre:['',[Validators.required]],
    precio:['',[ Validators.pattern('[0-9.]*'),
    Validators.required]],
    stock:['',[ Validators.pattern('[0-9.]*'),
    Validators.required]]
  })
  ngOnInit(): void {
  }

  Guardar(): void{
  Swal.fire({toast:true,padding:'1.5em',icon:'success',title:'Nuevo producto creado',
  timerProgressBar:true,showConfirmButton:false,timer:1000,iconColor:'blue',background:'#e5fbff'})
    this.service.CrearProducto(this.productoForm.value).subscribe(res =>{
      this.route.navigate(['producto-list']);
    });
    this.productoForm.reset();
  }
}
