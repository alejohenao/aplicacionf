export interface IProducto{
    id?:number;
    codigo?:string;
    nombre?:string;
    precio?:string;
    stock?:string;
}
export class Producto implements IProducto{
    constructor(
        public id?:number,
        public codigo?:string,
        public nombre?:string,
        public precio?:string,
        public stock?:string
    ){}
}
