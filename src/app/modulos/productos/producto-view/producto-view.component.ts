import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { IProducto } from '../Producto';
import { ProductoService } from '../producto.service';

@Component({
  selector: 'app-producto-view',
  templateUrl: './producto-view.component.html',
  styleUrls: ['./producto-view.component.styl']
})
export class ProductoViewComponent implements OnInit {
  producto: IProducto;
  searchForm = this.fb.group({
    codigo:['',[Validators.pattern('[0-9.]*')]]
  });
  constructor(private fb: FormBuilder,private service:ProductoService) { }

  ngOnInit(): void {
  }

  searchProducto(): void{
    console.warn('codigo', this.searchForm.value.codigo);
    this.service.BuscarCodigoProducto(this.searchForm.value.codigo)
    .subscribe(res => {
      console.warn('respuesta', res);
      this.producto = res[0]; 
    });
  }
}
