import { Component, OnInit } from '@angular/core';
import { IProducto } from '../Producto';
import { ProductoService } from '../producto.service';

@Component({
  selector: 'app-producto-list',
  templateUrl: './producto-list.component.html',
  styleUrls: ['./producto-list.component.styl']
})
export class ProductoListComponent implements OnInit {
  p: number=1;
  listadoProductos:IProducto[];
  
  constructor(private service:ProductoService) { }
  
  ngOnInit(): void {
    this.service.ListarProducto().subscribe(res =>{
      console.warn('mensaje de listado',res);
      this.listadoProductos=res;
    });
  }

  Eliminar(id:number){
    this.service.EliminarProducto(id).subscribe(() =>{
      this.ngOnInit();
    });
  }
}
