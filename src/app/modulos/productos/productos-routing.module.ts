import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductoCreateComponent } from './producto-create/producto-create.component';
import { ProductoListComponent } from './producto-list/producto-list.component';
import { ProductoUpdateComponent } from './producto-update/producto-update.component';
import { ProductoViewComponent } from './producto-view/producto-view.component';
import { ProductoComponent } from './producto.component';

const routes: Routes = [
{
  path:'producto-create',
  component:ProductoCreateComponent
},
{
  path:'producto-list',
  component:ProductoListComponent
},
{
  path:'producto-update',
  component:ProductoUpdateComponent
},
{
path:'producto-view',
component:ProductoViewComponent
},
{
  path:'producto',
  component:ProductoComponent
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductosRoutingModule { }
