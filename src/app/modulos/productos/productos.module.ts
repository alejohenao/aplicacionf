import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductosRoutingModule } from './productos-routing.module';
import { ProductoCreateComponent } from './producto-create/producto-create.component';
import { ProductoListComponent } from './producto-list/producto-list.component';
import { ProductoUpdateComponent } from './producto-update/producto-update.component';
import { ProductoComponent } from './producto.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductoViewComponent } from './producto-view/producto-view.component';
import { NgxPaginationModule } from 'ngx-pagination';


@NgModule({
  declarations: [
    ProductoCreateComponent,
    ProductoListComponent,
    ProductoUpdateComponent,
    ProductoComponent,
    ProductoViewComponent
  ],
  imports: [
    CommonModule,
    ProductosRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule
  ],
  exports:[
    ProductoCreateComponent,
    ProductoListComponent,
    ProductoUpdateComponent,
    ProductoComponent
  ]
})
export class ProductosModule { }
