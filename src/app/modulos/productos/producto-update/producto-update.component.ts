import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { IProducto } from '../Producto';
import { ProductoService } from '../producto.service';

@Component({
  selector: 'app-producto-update',
  templateUrl: './producto-update.component.html',
  styleUrls: ['./producto-update.component.styl']
})
export class ProductoUpdateComponent implements OnInit {
formProductoUpdate:FormGroup;
  constructor(   
    private activatedRoute: ActivatedRoute,
    private services: ProductoService,
    private formBuilder: FormBuilder,
    private router: Router) {
      this.formProductoUpdate = this.formBuilder.group({
        id:[],
        codigo:[''],
        nombre:[''],
        precio:[''],
        stock:['']
      });
     }

  ngOnInit(): void{
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    console.warn('id',id)
    this.services.ListarId(id)
    .subscribe(res => {
      console.warn('mensaje de id',res);
      this.loadForm(res);
    }, res => {});
  }
  //funcion para traer y carga los datos de personas
  private loadForm(producto:IProducto){
    this.formProductoUpdate.patchValue({
      id:producto.id,
      codigo:producto.codigo,
      nombre:producto.nombre,
      precio:producto.precio,
      stock:producto.stock
    });
  }
  //metodo para actualizar
  Actualizar():void{
    Swal.fire({toast:true,padding:'1.5em',icon:'success',title:'actualizacion realizada',
    timerProgressBar:true,showConfirmButton:false,timer:1500,iconColor:'blue',background:'#e5fbff'})
    this.services.ActualizarProducto(this.formProductoUpdate.value)
    .subscribe(res =>{
      console.warn('actualiza',res);
      this.router.navigate(['./homepage/menu/productos/producto-list']);
    },err => {});
  }

}
