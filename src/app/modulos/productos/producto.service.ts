import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { createRequestParams } from 'src/app/utils/utils';
import { environment } from 'src/environments/environment';
import { IProducto } from './Producto';
import { HttpClient, HttpParams } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  constructor(private http:HttpClient) { }
 //metodo para listar personas se comunica con el backend
 ListarProducto():Observable <IProducto[]>{
  return this.http.get<IProducto[]>(`${environment.END_POINT}/api/producto`)
  .pipe(map(res => {
      return res;
    })); 
    }

//metodo para crear personas se comunica con el backend
public CrearProducto(producto: IProducto):Observable<IProducto>{
  return this.http.post<IProducto>(`${environment.END_POINT}/api/producto`,producto)
  .pipe(map(res => {
   console.warn('producto',producto)
   return res;       
  }));
  }; 
//metodo para actualizar personas se comunica con el backend
ActualizarProducto(producto:IProducto):Observable<IProducto>{
return this.http.put<IProducto>(`${environment.END_POINT}/api/producto`,producto)
.pipe(map(res =>{
return res;
}));
}
//metodo para listar id personas se comunica con el backend
ListarId(id: string):Observable<IProducto>{
return this.http.get<IProducto>(`${environment.END_POINT}/api/producto/${id}`)
.pipe(map(res => {
  return res;
}));
}
//metodo para eliminar personas se comunica con el backend
EliminarProducto(id:number):Observable<IProducto>{
return this.http.delete<IProducto>(`${environment.END_POINT}/api/producto/${id}`)
.pipe(map(res =>{
  return res;
}));
}
//metodo para listar por documento se comunica con el backend
public BuscarCodigoProducto(codigo: string): Observable<IProducto[]>{
let params: HttpParams = new HttpParams();
params = params.set('codigo', codigo);
return this.http.get<IProducto[]>(`${environment.END_POINT}/api/producto/buscarCodigoProducto`,{params: params})
.pipe(map(res =>{
  return res;
}));
}
}
