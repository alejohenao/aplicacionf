import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MenuComponent } from './menu/menu.component';

const routes: Routes = [
  {
    path:'menu',
    component:MenuComponent,
    children:[
      {
        path:'usuarios',
        loadChildren:() => import('../modulos/usuarios/usuarios.module')
        .then(m=> m.UsuariosModule)
      },
      {
        path:'productos',
        loadChildren:() => import('../modulos/productos/productos.module')
        .then(m=> m.ProductosModule)
      },
      {
        path:'compras',
        loadChildren:()=> import('../modulos/compras/compras.module')
        .then(m=> m.ComprasModule)
      }
    ]
  },
  {
    path:'',
    redirectTo: 'menu',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomepageRoutingModule { }
