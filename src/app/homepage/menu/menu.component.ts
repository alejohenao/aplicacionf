import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.styl']
})
export class MenuComponent implements OnInit { 
  //variable para que la imagen quede solo al principio
  verImagen= false;
  constructor(private route:Router) {}



  ngOnInit(): void {
    //variable para que la imagen quede solo al principio
    this.verImagen=false;
   if(this.route.url==='/homepage/menu'){
     this.verImagen=true;     
   }
   else{this.verImagen=false};
  };

  
}
