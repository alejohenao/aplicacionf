import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomepageRoutingModule } from './homepage-routing.module';
import { UsuariosModule } from '../modulos/usuarios/usuarios.module';
import { NavbarComponent } from './navbar/navbar.component';
import { MenuComponent } from './menu/menu.component';
import { FooterComponent } from './footer/footer.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { SidebarModule } from 'ng-sidebar';
import { ProductosModule } from '../modulos/productos/productos.module';
import {MatMenuModule} from '@angular/material/menu';
import { CardsComponent } from './cards/cards.component';
import { ComprasModule } from '../modulos/compras/compras.module';






@NgModule({
  declarations: [NavbarComponent,MenuComponent, FooterComponent,SidebarComponent, CardsComponent],
  imports: [
    CommonModule,
    HomepageRoutingModule,
    UsuariosModule,
    SidebarModule,
    ProductosModule,
    MatMenuModule,
    ComprasModule
  ],
  exports:[
    CardsComponent
  ]
})
export class HomepageModule { }
